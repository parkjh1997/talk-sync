package com.yulisu.chat;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.reactive.config.EnableWebFlux;

import java.util.Optional;

@SpringBootApplication(scanBasePackages = "com.yulisu", exclude = { SecurityAutoConfiguration.class, ThymeleafAutoConfiguration.class })
@EnableWebFlux
@EnableAsync(proxyTargetClass = true)
@Slf4j
public class ChatApplication implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) {
        final Optional<String> cmdLine = ProcessHandle.current().info().commandLine();
        log.info("Start\t{}\t{}", ProcessHandle.current().pid(), cmdLine.isPresent() ? cmdLine.get() : "");
    }

    public static void main(String[] args) {
        SpringApplication.run(ChatApplication.class, args);
    }
}
