package com.yulisu.chat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Mono;

@Controller
//@RequiredArgsConstructor
public class ChatController {

//    private final SimpMessagingTemplate simpMessagingTemplate;
//
//    @MessageMapping("/sendMessage")
//    public void sendMessage(String message) {
//        simpMessagingTemplate.convertAndSend("/topic/receiveMessage", message);
//    }

    @GetMapping("/")
    public Mono<Rendering> home() {
        return Mono.just(Rendering.view("index").build());
    }
}
