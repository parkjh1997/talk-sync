package com.yulisu.chat.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

@Component
@RequiredArgsConstructor
@Slf4j
public class CustomWebSocketHandler implements WebSocketHandler {

    private final Sinks.Many<String> sink;

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        session.receive()
            .map(event -> event.getPayloadAsText())
            .map(event -> {
                try {
                    // 메시지 파싱
                    JSONObject json = new JSONObject(event);
                    String username = json.getString("username");
                    if (username.equals("")) username = "익명";
                    String message = json.getString("message");

                    log.info("[Message] - ", username + ": " + message);

                    return username + ": " + message;
                } catch (JSONException je) {
                    je.printStackTrace();
                    return "메시지 처리 중 오류 발생";
                }
            })
            .subscribe(s -> sink.emitNext(s, Sinks.EmitFailureHandler.FAIL_FAST));

        return session.send(sink.asFlux().map(session::textMessage));
    }
}
