description = "Chat API Server"

plugins {
    id("application")
    kotlin("jvm")
}

application {
    mainClass.set("com.yulisu.chat.ChatApplication")
    applicationDefaultJvmArgs = listOf(
            "-server",
            "-Djava.security.egd=file:/dev/./urandom",
            "-Djava.net.preferIPv4Stack=true",
            "-Djava.awt.headless=true",
            "-Dreactor.netty.http.server.accessLogEnabled=true"
    )
}

tasks.distTar {
    into("${project.name}-${project.version}") {
        from(".")
        include("config/*")
    }
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xdiags:verbose")
    options.compilerArgs.add("-parameters")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

dependencies {
    implementation(project(":ts-common"))

//    implementation("org.springframework.boot:spring-boot-starter-websocket")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")

    //json
    implementation("org.json:json:20231013")

    testImplementation("junit:junit")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("io.projectreactor:reactor-test")
}