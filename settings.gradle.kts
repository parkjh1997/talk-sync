rootProject.name = "talk-sync-api"

include(":ts-chat")
include(":ts-common")
include(":ts-web")

project(":ts-chat").projectDir = file("$rootDir/chat")
project(":ts-common").projectDir = file("$rootDir/common")
project(":ts-web").projectDir = file("$rootDir/web")