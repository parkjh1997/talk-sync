package com.yulisu.common.utils;

import lombok.Getter;

import javax.validation.constraints.Null;

/** 빈 값일 때 발생하는 예외<p>
 * @see Validator
 */
public class EmptyValueException extends RuntimeException
{
	private static final long serialVersionUID = -4001154964403512217L;
	@Getter
	private final Object name;

	public EmptyValueException() { this(null); }

	public EmptyValueException(Object name) {
		super(name == null ? "Value is empty." : String.format("\"%s\" named value is empty.", name));
		this.name = name;
	}

	@Null
	public Object getKey() { return this.name; }
}
