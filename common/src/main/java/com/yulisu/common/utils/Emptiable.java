package com.yulisu.common.utils;

/** {@link #isEmpty()} 메소드를 제공한다.
 * @see Validator
 */
public interface Emptiable
{
	boolean isEmpty();
}
