package com.yulisu.common.utils;

import com.google.common.collect.Multimap;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** 빈 값을 검사하는 클래스<p>
 * 여기서는 기본적인 "빈 값"(정의는 아래 참조)에 대한 검사 기능을 제공한다.<br>
 * 메소드가 많지만 대부분 overloading 메소드들이며, 아래 6가지 유형의 메소드만 존재한다.<br>
 * [주의] 공통적으로 <code>String</code> 자료형은 기본 trim 한 값을 대상으로 검사 또는 반환한다.<br>
 * <ul>
 * <li>Empty 관련 기능 : 모든 클래스 대상
 * <ul>
 * <li><code>isEmpty</code> : 빈 값인지 확인
 * <li><code>isNotEmpty</code> : 빈 값 아닌지 확인
 * <li><code>adjustEmpty</code> : Empty 값인 경우, 값을 조정한다 - Empty 값이 아니면 원래 값을 반환하고,
 * 빈 값이면 사용자가 전달한 default 값을 반환한다.
 * <li><code>ensureNotEmpty</code> : 빈 값이 아니면 원래 값을 반환하고,
 * 빈 값이면 EmptyValueException 을 던진다, 단 default 값이 존재하면 이 값을 리턴한다.
 * 그런데, 이 default 값 마저도 빈 값이면, EmptyValueException 을 던진다.
 * 이 메소드는 빈 값을 절대 리턴하지 않는다.
 * </ul>
 * <li>Blank 관련 기능 : <code>String</code>, <code>CharSequence</code> 대상
 * <ul>
 * <li><code>isBlank</code>
 * <li><code>isNotBlank</code>
 * <li><code>adjustBlank</code>
 * <li><code>ensureNotBlank</code>
 * </ul>
 * <li>Null safe 유틸리티
 * <ul>
 * <li><code>trim</code> : nullable 한 <code>String</code>을 trim 한다.
 * <li><code>toInt</code>, <code>toLong</code> : Exception 던지지 않고 숫자 파싱, Exception 발생시 기본값 전달.
 * </ul>
 * <li>Empty 정의는 유형별로 아래와 같다. ({@link javax.validation.constraints.NotEmpty} 에 사용되는 의미와 동일)
 * <ul>
 * <li>Containter 객체 : null 이거나 크기가 0 인 경우 (예: 배열, <code>Collection</code>, <code>Map</code>, <code>Iterable</code> ..)
 * <li>문자열 객체 : null 이거나 길이가 0 인 경우 (예: <code>String</code>, <code>CharSequence</code>)
 * <li>그 외 객체 : null 인 경우
 * </ul>
 * <li>문자열 객체에 한해 추가적으로 존재하는 Blank 정의는 아래와 같다. ({@link javax.validation.constraints.NotBlank} 에 사용되는 의미와 동일)
 * <ul>
 * <li>null 이거나, 문자열이 모두 공백(whitespce) 문자만 존재하는 경우
 * </ul>
 * </ul>
 * @see EmptyValueException
 * @see javax.validation.constraints.NotEmpty
 * @see javax.validation.constraints.NotBlank
 */
@Slf4j
public class Validator
{
	private static final Pattern P_NON_WS = Pattern.compile("\\S+");
	private static final Pattern P_TRIM = Pattern.compile("^\\s*(\\S|\\S.*\\S)\\s*$"); // 1: trimed string

	public static boolean isBlank(String v) { return v == null || v.length() == 0 || v.isBlank(); }
	public static <T extends CharSequence> boolean isBlank(T v) { return v == null || v.length() == 0 || !P_NON_WS.matcher(v).find(); }

	public static <T> boolean isNull(T v) { return v == null; }
	public static <T> boolean isNotNull(T v) { return v != null; }
	public static <T> T adjustNull(T v, T def) { return v != null ? v : def; }
	public static <T> T adjustNull(T v, Supplier<T> supp) { return v != null ? v : supp.get(); }
	@NotNull public static <T> T ensureNotNull(T v) throws EmptyValueException { if (v != null) return v; throw new EmptyValueException(); }
	@NotNull public static <T> T ensureNotNull(T v, T def) throws EmptyValueException { return v != null ? v : ensureNotNull(def); }
	@NotNull public static <T> T ensureNotNull(T v, Supplier<T> supp) throws EmptyValueException { return v != null ? v : ensureNotNull(supp.get()); }

	public static <T> boolean isEmpty(T v) { return v == null; }
	public static <T extends CharSequence> boolean isEmpty(T v) { return v == null || v.length() == 0; }
	public static <T extends Emptiable> boolean isEmpty(T v) { return v == null || v.isEmpty(); }
	public static <T extends Iterable<?>> boolean isEmpty(T v) { return v == null || !v.iterator().hasNext(); }
	public static <T extends Collection<?>> boolean isEmpty(T v) { return v == null || v.size() == 0; }
	public static <T extends Map<?, ?>> boolean isEmpty(T v) { return v == null || v.size() == 0; }
	public static <T extends Multimap<?, ?>> boolean isEmpty(T v) { return v == null || v.size() == 0; }
	public static boolean isEmpty(Object[] ao) { return ao == null || ao.length == 0; }
	public static boolean isEmpty(byte[] ab) { return ab == null || ab.length == 0; }
	public static boolean isEmpty(short[] ash) { return ash == null || ash.length == 0; }
	public static boolean isEmpty(int[] ai) { return ai == null || ai.length == 0; }
	public static boolean isEmpty(long[] al) { return al == null || al.length == 0; }
	public static boolean isEmpty(float[] af) { return af == null || af.length == 0; }
	public static boolean isEmpty(double[] ad) { return ad == null || ad.length == 0; }

	public static boolean isNotBlank(String v) { return !isBlank(v); }
	public static <T extends CharSequence> boolean isNotBlank(T v) { return !isBlank(v); }

	public static boolean existsAnyNonEmptyMember(String...as) {
		if (as != null) {
			for (String s : as) {
				if (s != null && s.length() > 0) {
					return true;
				}
			}
		}
		return false;
	}

	public static <T> boolean isNotEmpty(T v) { return v != null; }
	public static <T extends CharSequence> boolean isNotEmpty(T v) { return v != null && v.length() > 0; }
	public static <T extends Emptiable> boolean isNotEmpty(T v) { return v != null && !v.isEmpty(); }
	public static <T extends Iterable<?>> boolean isNotEmpty(T v) { return v != null && v.iterator().hasNext(); }
	public static <T extends Collection<?>> boolean isNotEmpty(T v) { return v != null && v.size() > 0; }
	public static <T extends Map<?, ?>> boolean isNotEmpty(T v) { return v != null && v.size() > 0; }
	public static <T extends Multimap<?, ?>> boolean isNotEmpty(T v) { return v != null && v.size() > 0; }
	public static boolean isNotEmpty(Object[] ao) { return ao != null && ao.length > 0; }
	public static boolean isNotEmpty(byte[] ab) { return ab != null && ab.length > 0; }
	public static boolean isNotEmpty(short[] ash) { return ash != null && ash.length > 0; }
	public static boolean isNotEmpty(int[] ai) { return ai != null && ai.length > 0; }
	public static boolean isNotEmpty(long[] al) { return al != null && al.length > 0; }
	public static boolean isNotEmpty(float[] af) { return af != null && af.length > 0; }
	public static boolean isNotEmpty(double[] ad) { return ad != null && ad.length > 0; }

	@NotNull public static String ensureNotBlank(String v) throws EmptyValueException { if ((v = trim(v, null)) != null) return v; throw new EmptyValueException(); }
//	@NotNull public static <V extends CharSequence> String ensureNotBlank(V v) throws EmptyValueException { String s; if ((s = trim(v, null)) == null) throw new EmptyValueException(); else return s; }

//	@NotNull public static String ensureNotEmpty(String v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static <V extends CharSequence> V ensureNotEmpty(V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static <V extends Emptiable> V ensureNotEmpty(V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static <V extends Iterable<?>> V ensureNotEmpty(V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static <V extends Collection<?>> V ensureNotEmpty(V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static <V extends Map<?,?>> V ensureNotEmpty(V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static <V> V ensureNotEmpty(V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static <V> V[] ensureNotEmpty(V[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static byte[] ensureNotEmpty(byte[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static short[] ensureNotEmpty(short[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static int[] ensureNotEmpty(int[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static long[] ensureNotEmpty(long[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static float[] ensureNotEmpty(float[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }
	@NotNull public static double[] ensureNotEmpty(double[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(); else return v; }

	@NotNull public static String ensureNotBlank(String v, String def) throws EmptyValueException { return isBlank(v) ? ensureNotBlank(def) : v; }
	@NotNull public static String ensureNotBlank(String v, Supplier<String> supp) throws EmptyValueException {
		return (v = trim(v, null)) == null ? ensureNotBlank(supp.get()) : v;
	}
//	@NotNull public static <V extends CharSequence> String ensureNotBlank(V v, Supplier<String> supp) throws EmptyValueException {
//		final String s = trim(v, null);
//		return s == null ? ensureNotBlank(supp.get()) : v.toString();
//	} // ensureNotBlank()

	@NotNull public static String ensureNotEmpty(String v, String def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
//	@NotNull public static <V extends CharSequence> V ensureNotEmpty(V v, V def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static <V extends Emptiable> V ensureNotEmpty(V v, V def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static <V extends Iterable<?>> V ensureNotEmpty(V v, V def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static <V extends Collection<?>> V ensureNotEmpty(V v, V def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static <V extends Map<?, ?>> V ensureNotEmpty(V v, V def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static <V> V ensureNotEmpty(V v, V def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static <V> V[] ensureNotEmpty(V[] v, V[] def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static byte[] ensureNotEmpty(byte[] v, byte[] def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static short[] ensureNotEmpty(short[] v, short[] def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static int[] ensureNotEmpty(int[] v, int[] def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static long[] ensureNotEmpty(long[] v, long[] def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static float[] ensureNotEmpty(float[] v, float[] def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }
	@NotNull public static double[] ensureNotEmpty(double[] v, double[] def) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(def) : v; }

//	@NotNull public static String ensureNotEmpty(String v, Supplier<String> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static <V extends CharSequence> V ensureNotEmpty(V v, Supplier<V> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static <V extends Emptiable> V ensureNotEmpty(V v, Supplier<V> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static <V extends Iterable<?>> V ensureNotEmpty(V v, Supplier<V> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static <V extends Collection<?>> V ensureNotEmpty(V v, Supplier<V> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static <V extends Map<?, ?>> V ensureNotEmpty(V v, Supplier<V> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static <V> V ensureNotEmpty(V v, Supplier<V> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static <V> V[] ensureNotEmpty(V[] v, Supplier<V[]> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static byte[] ensureNotEmpty(byte[] v, Supplier<byte[]> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static short[] ensureNotEmpty(short[] v, Supplier<short[]> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static int[] ensureNotEmpty(int[] v, Supplier<int[]> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static long[] ensureNotEmpty(long[] v, Supplier<long[]> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static float[] ensureNotEmpty(float[] v, Supplier<float[]> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }
	@NotNull public static double[] ensureNotEmpty(double[] v, Supplier<double[]> supp) throws EmptyValueException { return isEmpty(v) ? ensureNotEmpty(supp.get()) : v; }

	@NotNull public static String ensureNamedNotBlank(Object oKey, String v) throws EmptyValueException {
		if ((v = trim(v, null)) == null) throw new EmptyValueException(oKey); else return v;
	}
	@NotNull public static <V extends CharSequence> String ensureNamedNotBlank(Object oKey, V v) throws EmptyValueException {
		final String s = trim(v, null);
		if (s == null) throw new EmptyValueException(oKey);
		else return s;
	}

	@NotNull public static String ensureNamedNotEmpty(Object k, String v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static <V extends CharSequence> V ensureNamedNotEmpty(Object k, V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static <V extends Emptiable> V ensureNamedNotEmpty(Object k, V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static <V extends Iterable<?>> V ensureNamedNotEmpty(Object k, V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static <V extends Collection<?>> V ensureNamedNotEmpty(Object k, V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static <V extends Map<?, ?>> V ensureNamedNotEmpty(Object k, V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static <V> V ensureNamedNotEmpty(Object k, V v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static <V> V[] ensureNamedNotEmpty(Object k, V[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static byte[] ensureNamedNotEmpty(Object k, byte[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static short[] ensureNamedNotEmpty(Object k, short[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static int[] ensureNamedNotEmpty(Object k, int[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static long[] ensureNamedNotEmpty(Object k, long[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static float[] ensureNamedNotEmpty(Object k, float[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }
	@NotNull public static double[] ensureNamedNotEmpty(Object k, double[] v) throws EmptyValueException { if (isEmpty(v)) throw new EmptyValueException(k); else return v; }

	@NotNull public static String ensureNamedNotBlank(Object oKey, String v, Function<Object, String> func) throws EmptyValueException {
		return (v = trim(v, null)) == null ? ensureNamedNotBlank(oKey, func.apply(oKey)) : v;
	}
	@NotNull public static <V extends CharSequence> String ensureNamedNotBlank(Object oKey, V v, Function<Object, String> func) throws EmptyValueException {
		final String s = trim(v, null);
		return s == null ? ensureNamedNotBlank(oKey, func.apply(oKey)) : s;
	}
	@NotNull public static String ensureNamedNotEmpty(Object oKey, String v, Function<Object, String> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static <K, V extends CharSequence> V ensureNamedNotEmpty(K oKey, V v, Function<K, V> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static <K, V extends Emptiable> V ensureNamedNotEmpty(K oKey, V v, Function<K, V> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static <K, V extends Iterable<?>> V ensureNamedNotEmpty(K oKey, V v, Function<K, V> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static <K, V extends Collection<?>> V ensureNamedNotEmpty(K oKey, V v, Function<K, V> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static <K, V extends Map<?, ?>> V ensureNamedNotEmpty(K oKey, V v, Function<K, V> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static <K, V> V ensureNamedNotEmpty(K oKey, V v, Function<K, V> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static <K, V> V[] ensureNamedNotEmpty(K oKey, V[] v, Function<K, V[]> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static byte[] ensureNamedNotEmpty(Object oKey, byte[] v, Function<Object, byte[]> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static short[] ensureNamedNotEmpty(Object oKey, short[] v, Function<Object, short[]> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static int[] ensureNamedNotEmpty(Object oKey, int[] v, Function<Object, int[]> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static long[] ensureNamedNotEmpty(Object oKey, long[] v, Function<Object, long[]> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static float[] ensureNamedNotEmpty(Object oKey, float[] v, Function<Object, float[]> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }
	@NotNull public static double[] ensureNamedNotEmpty(Object oKey, double[] v, Function<Object, double[]> func) throws EmptyValueException { return isEmpty(v) ? ensureNamedNotEmpty(oKey, func.apply(oKey)) : v; }

	public static String adjustBlank(String v) { return (v = trim(v, null)) == null ? null : v; }
	public static <V extends CharSequence> String adjustBlank(V v) { String s = trim(v, null); return s == null ? null : s; }

	public static String adjustEmpty(String v) { return isEmpty(v) ? null : v; }
	public static <V extends CharSequence> V adjustEmpty(V v) { return isEmpty(v) ? null : v; }
	public static <V extends Emptiable> V adjustEmpty(V v) { return isEmpty(v) ? null : v; }
	public static <V extends Iterable<?>> V adjustEmpty(V v) { return isEmpty(v) ? null : v; }
	public static <V extends Collection<?>> V adjustEmpty(V v) { return isEmpty(v) ? null : v; }
	public static <V extends Map<?, ?>> V adjustEmpty(V v) { return isEmpty(v) ? null : v; }
	public static <V> V adjustEmpty(V v) { return isEmpty(v) ? null : v; }
	public static <V> V[] adjustEmpty(V[] v) { return isEmpty(v) ? null : v; }
	public static byte[] adjustEmpty(byte[] v) { return isEmpty(v) ? null : v; }
	public static short[] adjustEmpty(short[] v) { return isEmpty(v) ? null : v; }
	public static int[] adjustEmpty(int[] v) { return isEmpty(v) ? null : v; }
	public static long[] adjustEmpty(long[] v) { return isEmpty(v) ? null : v; }
	public static float[] adjustEmpty(float[] v) { return isEmpty(v) ? null : v; }
	public static double[] adjustEmpty(double[] v) { return isEmpty(v) ? null : v; }
	@NotNull public static org.slf4j.Logger adjustEmpty(org.slf4j.Logger v) {
		return v == null ? VoidLogger.get() : v;
	}

	public static String adjustBlank(String v, String def) { return (v = trim(v, null)) != null ? v : def; }
//	public static <V extends CharSequence> String adjustBlank(V v, String def) { String s = trim(v, null); return s != null ? s : def; }

	public static String adjustEmpty(String v, String def) { return isEmpty(v) ? def : v; }
//	public static <V extends CharSequence> V adjustEmpty(V v, V def) { return isEmpty(v) ? def : v; }
	public static <V extends Emptiable> V adjustEmpty(V v, V def) { return isEmpty(v) ? def : v; }
	public static <V extends Iterable<?>> V adjustEmpty(V v, V def) { return isEmpty(v) ? def : v; }
	public static <V extends Collection<?>> V adjustEmpty(V v, V def) { return isEmpty(v) ? def : v; }
	public static <V extends Map<?,?>> V adjustEmpty(V v, V def) { return isEmpty(v) ? def : v; }
	public static <V> V adjustEmpty(V v, V def) { return isEmpty(v) ? def : v; }
	public static <V> V[] adjustEmpty(V[] v, V[] def) { return isEmpty(v) ? def : v; }
	public static byte[] adjustEmpty(byte[] v, byte[] def) { return isEmpty(v) ? def : v; }
	public static short[] adjustEmpty(short[] v, short[] def) { return isEmpty(v) ? def : v; }
	public static int[] adjustEmpty(int[] v, int[] def) { return isEmpty(v) ? def : v; }
	public static long[] adjustEmpty(long[] v, long[] def) { return isEmpty(v) ? def : v; }
	public static float[] adjustEmpty(float[] v, float[] def) { return isEmpty(v) ? def : v; }
	public static double[] adjustEmpty(double[] v, double[] def) { return isEmpty(v) ? def : v; }

	public static String adjustBlank(String v, Supplier<String> supp) { return (v = trim(v, null)) != null ? v : supp.get(); }
//	public static <V extends CharSequence> String adjustBlank(V v, Supplier<String> supp) { String s = trim(v, null); return s != null ? s : supp.get(); }

	public static String adjustEmpty(String v, Supplier<String> supp) { return isEmpty(v) ? supp.get() : v; }
//	public static <V extends CharSequence> V adjustEmpty(V v, Supplier<V> supp) { return isEmpty(v) ? supp.get() : v; }
	public static <V extends Emptiable> V adjustEmpty(V v, Supplier<V> supp) { return isEmpty(v) ? supp.get() : v; }
	public static <V extends Iterable<?>> V adjustEmpty(V v, Supplier<V> supp) { return isEmpty(v) ? supp.get() : v; }
	public static <V extends Collection<?>> V adjustEmpty(V v, Supplier<V> supp) { return isEmpty(v) ? supp.get() : v; }
	public static <V extends Map<?,?>> V adjustEmpty(V v, Supplier<V> supp) { return isEmpty(v) ? supp.get() : v; }
	public static <V> V adjustEmpty(V v, Supplier<V> supp) { return isEmpty(v) ? supp.get() : v; }
	public static <V> V[] adjustEmpty(V[] v, Supplier<V[]> supp) { return isEmpty(v) ? supp.get() : v; }
	public static byte[] adjustEmpty(byte[] v, Supplier<byte[]> supp) { return isEmpty(v) ? supp.get() : v; }
	public static short[] adjustEmpty(short[] v, Supplier<short[]> supp) { return isEmpty(v) ? supp.get() : v; }
	public static int[] adjustEmpty(int[] v, Supplier<int[]> supp) { return isEmpty(v) ? supp.get() : v; }
	public static long[] adjustEmpty(long[] v, Supplier<long[]> supp) { return isEmpty(v) ? supp.get() : v; }
	public static float[] adjustEmpty(float[] v, Supplier<float[]> supp) { return isEmpty(v) ? supp.get() : v; }
	public static double[] adjustEmpty(double[] v, Supplier<double[]> supp) { return isEmpty(v) ? supp.get() : v; }

	public static <E extends Enum<E>> E adjustEmpty(Class<E> clazz, String name, E eDefault) {
		if (name != null) {
			try { return Enum.valueOf(clazz, name); } catch (IllegalArgumentException | NullPointerException e) {}
		} // if
		return eDefault;
	}

	@NotNull public static String trim(String v) { return trim(v, ""); }
	@NotNull public static <V extends CharSequence>  String trim(V v) { return trim(v, ""); }
	public static String trim(String v, String sDefaultIfEmpty) {
		return v == null || v.isEmpty() || (v = v.trim()).isEmpty() ? sDefaultIfEmpty : v;
	}
	public static <V extends CharSequence> String trim(V v, String sDefaultIfEmpty) {
		Matcher m;;
		return (v == null || !(m = P_TRIM.matcher(v)).matches()) ? sDefaultIfEmpty : m.group(1);
	}

	public static int toInt(String s) { return toInt(s, () -> 0); }
	public static int toInt(String s, int iDefault) { return toInt(s, () -> iDefault); }
	public static int toInt(String s, Supplier<Integer> supp) {
		if ((s = adjustEmpty(s)) != null) {
			try {
				return Integer.parseInt(s);
			} catch (Exception e) {
			} // try
		} // if
		return supp.get();
	} // toInt()

	public static long toLong(String s) { return toLong(s, () -> 0L); }
	public static long toLong(String s, long lDefault) { return toLong(s, () -> lDefault); }
	public static long toLong(String s, Supplier<Long> supp) {
		if ((s = adjustEmpty(s)) != null) {
			try {
				return Long.parseLong(s);
			} catch (Exception e) {
			} // try
		} // if
		return supp.get();
	} // toLong()

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean isTypeOf(Object o, Class<?> clazz) {
		if (clazz.isEnum() && o instanceof String) {
			try {
				Enum.valueOf((Class<Enum>) clazz, (String) o);
				return true;
			} catch (IllegalArgumentException e) {
				return false;
			} // try
		} else {
			return clazz.isAssignableFrom(o.getClass());
		} // if
	} // isTypeOf()

	/** org.apache.commons.lang3.StringUtils.equals(CharSequence, CharSequence)
	 * <pre>
	 * Validator.equals(null, null)   = true
	 * Validator.equals(null, "abc")  = false
	 * Validator.equals("abc", null)  = false
	 * Validator.equals("abc", "abc") = true
	 * Validator.equals("abc", "ABC") = false
	 * </pre>
	 */
	public static boolean equals(final CharSequence cs1, final CharSequence cs2) {
		if (cs1 == cs2) {
			return true;
		} else if (cs1 == null || cs2 == null) {
			return false;
		} else if (cs1.length() != cs2.length()) {
			return false;
		} else if (cs1 instanceof String && cs2 instanceof String) {
			return cs1.equals(cs2);
		} else {
			// Step-wise comparison
			final int length = cs1.length();
			for (int i = 0; i < length; i++) {
				if (cs1.charAt(i) != cs2.charAt(i)) {
					return false;
				}
			}
			return true;
		} // if
	} // equals()

	public static boolean equals(final String s1, final String s2) {
		return s1 == s2
			|| s1 != null && s1.equals(s2)
			;
	} // equals()

	public static boolean equals(final Object o1, final Object o2) {
		return o1 == o2
			|| o1 != null && o1.equals(o2)
			;
	} // equals()

	/** 내부 element 값들을 탐색해서, 빈 값의 element를 모두 제거한다. 결국 Collection 크기가 0가 되면, null을 반환한다.
	 * @param <T>
	 * @param c
	 * @return
	 */
	@Nullable
	public static <T extends Collection<?>> T adjustDeepEmpty(T c) {
		if (Validator.isEmpty(c)) {
			return null;
		} else {
			for (final Iterator<?> it = c.iterator(); it.hasNext(); ) {
				final Object oValue = it.next();
				if (oValue == null) {
					it.remove();
				} else if (oValue instanceof String) {
					if (((String) oValue).isEmpty()) {
						it.remove();
					}
				} else if (oValue instanceof Collection) {
					if (Validator.adjustDeepEmpty((Collection<?>) oValue) == null) {
						it.remove();
					}
				} else if (oValue instanceof Map) {
					if (Validator.adjustDeepEmpty((Map<?, ?>) oValue) == null) {
						it.remove();
					}
				}
			} // for
			return Validator.adjustEmpty(c);
		} // if
	}

	/** 내부 entry 값들을 탐색해서, 빈 값을 가지는 entry를 모두 제거한다. 결국 Map 크기가 0가 되면, null을 반환한다.
	 * @param <T>
	 * @param m
	 * @return
	 */
	@Nullable
	public static <T extends Map<?, ?>> T adjustDeepEmpty(T m) {
		if (Validator.isEmpty(m)) {
			return null;
		} else {
			for (final Iterator<?> it = m.values().iterator(); it.hasNext(); ) {
				final Object oValue = it.next();
				if (oValue == null) {
					it.remove();
				} else if (oValue instanceof String) {
					if (((String) oValue).isEmpty()) {
						it.remove();
					}
				} else if (oValue instanceof Collection) {
					if (Validator.adjustDeepEmpty((Collection<?>) oValue) == null) {
						it.remove();
					}
				} else if (oValue instanceof Map) {
					if (Validator.adjustDeepEmpty((Map<?, ?>) oValue) == null) {
						it.remove();
					}
				}
			} // for
			return Validator.adjustEmpty(m);
		} // if
	}
}