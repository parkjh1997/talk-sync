package com.yulisu.common.utils;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.spi.FilterReply;
import lombok.Setter;

@Setter
public class AccessLogFilter extends AbstractMatcherFilter<ILoggingEvent>
{
	private String method; // argumentArray[3]
	private String path; // argumentArray[4]

	public void setMethod(String method) {
		method = Validator.adjustBlank(method);
		this.method = method == null ? null : method.toUpperCase();
	}

	public void setPath(String path) {
		this.path = Validator.adjustBlank(path);
	}

	@Override
	public FilterReply decide(ILoggingEvent event) {
		if (!isStarted()) {
			return FilterReply.NEUTRAL;
		}
		final Object[] ao = event.getArgumentArray();
		return ao.length > 4
			&& (this.method == null || ao[3] != null && ao[3].equals(this.method))
			&& (this.path == null || ao[4] != null && ((String) ao[4]).startsWith(this.path))
			? super.onMatch
			: super.onMismatch
			;
	} // deside()

	public void start() {
		if (this.method != null || this.path != null) {
			super.start();
		}
	}
}
