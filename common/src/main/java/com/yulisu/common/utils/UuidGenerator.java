package com.yulisu.common.utils;

import org.apache.commons.codec.binary.Base64;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

/** Base64 url-safe 형식으로 인코딩한 22byte UUID (ID Space = 2^128 = 3.402823669209385e38)
 */
public final class UuidGenerator implements java.io.Serializable
{
	private static final long serialVersionUID = 8834473340311395681L;
	private static final int defaultSize = 22;

	/*
	 * The random number generator used by this class to create random
	 * based UUIDs. In a holder class to defer initialization until needed.
	 */
	private static class Holder {
		static final SecureRandom numberGenerator = new SecureRandom();
	}

	/** Base64 Safe URL 형식의 랜덤 ID */
//	public static String next() {
//		return next(null);
//	}

//	public static String next(Integer size) {
//		final byte[] ab = Base64.encodeBase64URLSafe(randomBytes());
//		return new String(ab, 0, size != null && size > 0 && size < defaultSize ? size : defaultSize, StandardCharsets.ISO_8859_1);
//	}

	/** 알파벳 + 숫자로만 구성된 랜덤 ID */
//	public static String nextNoSpecial() {
//		return nextNoSpecial(null);
//	}

	public static String nextNoSpecial(Integer size) {
		final byte[] ab = _next(Base64.encodeBase64URLSafe(randomBytes()), true, false);
		return new String(ab, 0, size != null && size > 0 && size < defaultSize ? size : defaultSize, StandardCharsets.ISO_8859_1);
	}

//	public static String nextNoSpecialLower() {
//		return nextNoSpecialLower(null);
//	}

//	/** 소문자 + 숫자로만 구성된 랜덤 ID */
//	public static String nextNoSpecialLower(Integer size) {
//		final byte[] ab = _next(Base64.encodeBase64URLSafe(randomBytes()), true, true);
//		return new String(ab, 0, size != null && size > 0 && size < defaultSize ? size : defaultSize, StandardCharsets.ISO_8859_1);
//	}

//	private static String _nextString(byte[] randomBytes, boolean noSpecial, boolean lowerCase) {
//		return _nextString(null, randomBytes, noSpecial, lowerCase);
//	}

//	private static String _nextString(Integer size, byte[] randomBytes, boolean noSpecial, boolean lowerCase) {
//		final byte[] ab = _next(Base64.encodeBase64URLSafe(randomBytes), noSpecial, lowerCase);
//		return new String(ab, 0, size != null && size > 0 && size < defaultSize ? size : defaultSize, StandardCharsets.ISO_8859_1);
//	}

	/**
	 * @param lowerCase 영문 소문자만 사용
	 * @param noSpecial 알파벳 + 숫자만 사용
	 * @return
	 */
	private static byte[] _next(byte[] ab, boolean noSpecial, boolean lowerCase) {
		if (noSpecial || lowerCase) {
			for (int i = 0; i < ab.length; ++i) {
				final int c = ab[i];
				if (noSpecial && (c == '-' || c == '_')) {
					int iRandom = (int) (Math.random() * (lowerCase ? 36 : 62));
					if (iRandom < 10) { // number
						ab[i] = (byte) (iRandom + '0');
					} else if (iRandom < 36) { // lower case
						ab[i] = (byte) (iRandom - 10 + 'a');
					} else { // upper case
						ab[i] = (byte) (iRandom - 36 + 'A');
					} // if
				} else if (lowerCase && Character.isUpperCase(c)) {
					ab[i] = (byte) (c + ('a' - 'A'));
				} // if
			} // for
		} // if
		return ab;
	}

	/** Static factory to retrieve a type 4 (pseudo randomly generated) UUID.
	 * The {@code UUID} is generated using a cryptographically strong pseudo
	 * random number generator.
	 * @return  A randomly generated {@code UUID}
	 */
	private static byte[] randomBytes() {
		final SecureRandom ng = Holder.numberGenerator;
		final byte[] randomBytes = new byte[16];
		ng.nextBytes(randomBytes);
		return randomBytes;
	}
}
