package com.yulisu.common.utils;

import lombok.Getter;
import lombok.SneakyThrows;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.concurrent.locks.ReentrantLock;

/** Base64(URL Safe)형식의 SHA-1 Hash 문자열을 생성합니다.<p>
 * 생성 가능한 문자열 길이는 최대 27자입니다. (SHA-1 -> 20bytes -> 27 base64 chars)
 */
public class HashGenerator {
	private static final Encoder encoder = Base64.getUrlEncoder();
	private static final Decoder decoder = Base64.getUrlDecoder();

	private static final String AES_ALGORITHM = "AES";
	private static final String SECRET_KEY = "SOLUMON_SECRET_KEY";

	private final MessageDigest md;
	private final ReentrantLock lock;
	@Getter
	private final int charLengthLimit;
	@Getter
	private final int charLength;
	@Getter
	private final int byteLength;

	private static byte[] abEmpty;
	private static final byte[][] aabNull = new byte[16][];
	static {
		try {
			final MessageDigest md = MessageDigest.getInstance("SHA-1");
			abEmpty = md.digest();
			final byte[] ab = new byte[1];
			for (int i = 0; i < aabNull.length; ++i, md.reset()) {
				ab[0] = (byte) i;
				aabNull[i] = md.digest(ab);
			} // for
		} catch (NoSuchAlgorithmException e) { /* ignore */ }
	} // static

	/** Sphere Analytics 표준 22자 해쉬 코드 생성기 */
//	public static HashGenerator of() {
//		return new HashGenerator(22);
//	}

	public static HashGenerator of(int charLength) {
		return new HashGenerator(charLength);
	}

	@SneakyThrows(NoSuchAlgorithmException.class)
	private HashGenerator(int iB64CharSize) {
		this.md = MessageDigest.getInstance("SHA-1"); // 20 bytes -> 20*8=160 bits -> Math.ceil(160/6)=27 base64 chars
		this.lock = new ReentrantLock();
		this.charLengthLimit = (int) Math.ceil(20 * 8 / 6); // = 27
		this.charLength = iB64CharSize < this.charLengthLimit ? iB64CharSize : this.charLengthLimit;
		this.byteLength = (int) Math.ceil(this.charLength * 6 / 8);
	}

	/** 알파벳 + 숫자로만 구성된 랜덤 ID */
//	public String hashNoSpecialString(byte[]...values) {
//		final byte[] ab = HashGenerator._toNoSpecial(HashGenerator.encoder.encode(this.hash(values)));
//		return new String(ab, 0, this.charLength, StandardCharsets.ISO_8859_1);
//	}
//
//	public String hashNoSpecialString(Object...values) {
//		final byte[] ab = HashGenerator._toNoSpecial(HashGenerator.encoder.encode(this.hash(values)));
//		return new String(ab, 0, this.charLength, StandardCharsets.ISO_8859_1);
//	}
//
//	/** 알파벳 + 숫자로만 구성된 랜덤 ID */
//	public String hashNoSpecialString(Iterable<Object> values) {
//		final byte[] ab = HashGenerator._toNoSpecial(HashGenerator.encoder.encode(this.hash(values)));
//		return new String(ab, 0, this.charLength, StandardCharsets.ISO_8859_1);
//	}
//
//	/** 알파벳 + 숫자로만 구성된 랜덤 ID */
//	public String hashNoSpecialString(Map<String, ?> namedValues) {
//		final byte[] ab = HashGenerator._toNoSpecial(HashGenerator.encoder.encode(this.hash(namedValues)));
//		return new String(ab, 0, this.charLength, StandardCharsets.ISO_8859_1);
//	}

//	private static byte[] _toNoSpecial(byte[] ab) {
//		for (int i = 0, n = ab.length; i < n; ++i) {
//			final int c = ab[i];
//			if (c == '-' || c == '_') {
//				if (i < 26) {
//					ab[i] = (byte) (i + (c == '-' ? 'A' : 'a'));
//				} else {
//					ab[i] = (byte) (i - 26 + '0');
//				} // if
//			} // if
//		} // for
//		return ab;
//	}

//	public String hashString(byte[]...values) {
//		return new String(HashGenerator.encoder.encode(this.hash(values)), 0, this.charLength, StandardCharsets.ISO_8859_1);
//	}

//	public byte[] hash(byte[]...values) {
//		lock.lock();
//		md.reset();
//		for (int i = 0; i < values.length; ++i) {
//			md.update(values[i] == null
//				? aabNull[i]
//				: values[i].length == 0
//					? abEmpty
//					: values[i]
//				);
//		} // for
//		final byte[] digest = md.digest();
//		lock.unlock();
//		return digest;
//	}

//	public String hashString(Iterable<Object> values) {
//		return new String(HashGenerator.encoder.encode(this.hash(values)), 0, this.charLength, StandardCharsets.ISO_8859_1);
//	}

//	public byte[] hash(Iterable<Object> values) {
//		lock.lock();
//		md.reset();
//		int i = 0;
//		for (final Object value : values) {
//			final String s = value == null ? null : value.toString();
//			md.update(s == null
//				? aabNull[i]
//				: s.isEmpty()
//					? abEmpty
//					: s.getBytes(StandardCharsets.UTF_8)
//				);
//			++i;
//		} // for
//		final byte[] digest = md.digest();
//		lock.unlock();
//		return digest;
//	}

	public String hashString(Object...values) {
		return new String(HashGenerator.encoder.encode(this.hash(values)), 0, this.charLength, StandardCharsets.ISO_8859_1);
	}

	public byte[] hash(Object...values) {
		lock.lock();
		md.reset();
		for (int i = 0; i < values.length; ++i) {
			final String s = values[i] == null ? null : values[i].toString();
			md.update(s == null
				? aabNull[i]
				: s.isEmpty()
					? abEmpty
					: s.getBytes(StandardCharsets.UTF_8)
				);
		} // for
		final byte[] digest = md.digest();
		lock.unlock();
		return digest;
	}

//	public String hashString(Map<String, ?> namedValues) {
//		return new String(HashGenerator.encoder.encode(this.hash(namedValues)), 0, this.charLength, StandardCharsets.ISO_8859_1);
//	}

//	public byte[] hash(Map<String, ?> namedValues) {
//		lock.lock();
//		md.reset();
//		for (final Map.Entry<String, ?> namedValue : namedValues.entrySet()) {
//			if (namedValue.getValue() != null) {
//				md.update(namedValue.getKey().getBytes(StandardCharsets.UTF_8));
//				md.update(namedValue.getValue().toString().getBytes(StandardCharsets.UTF_8));
//			} // if
//		} // for
//		final byte[] digest = md.digest();
//		lock.unlock();
//		return digest;
//	}

	/** Base64(URL Safe) 문자열을 디코딩하여 바이트 배열을 반환합니다. */
	public byte[] decode(String base64String) {
		return decoder.decode(base64String);
	}

	/** Base64(URL Safe) 문자열을 디코딩하여 문자열을 반환합니다. */
	public String decodeToString(String base64String) {
		byte[] decodedBytes = decode(base64String);
		return new String(decodedBytes, StandardCharsets.ISO_8859_1);
//		return new String(decodedBytes, StandardCharsets.UTF_8);
	}

	private static String generateKey() {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] key = digest.digest(SECRET_KEY.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(key).substring(0, 32); // Use the first 32 bytes
		} catch (Exception e) {
			e.printStackTrace();
			// Handle the exception appropriately
			return null;
		}
	}

	public static String encrypt(String originalString) {
		try {
			String secretKey = generateKey();
			Cipher cipher = Cipher.getInstance(AES_ALGORITHM);
			SecretKey key = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), AES_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, key);

			byte[] encryptedBytes = cipher.doFinal(originalString.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(encryptedBytes);
		} catch (Exception e) {
			e.printStackTrace();
			// Handle the exception appropriately
			return null;
		}
	}

	public static String decrypt(String encryptedString) {
		try {
			String secretKey = generateKey();
			Cipher cipher = Cipher.getInstance(AES_ALGORITHM);
			SecretKey key = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), AES_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, key);

			byte[] decodedBytes = Base64.getDecoder().decode(encryptedString);
			byte[] decryptedBytes = cipher.doFinal(decodedBytes);
			return new String(decryptedBytes, StandardCharsets.UTF_8);
		} catch (Exception e) {
			e.printStackTrace();
			// Handle the exception appropriately
			return null;
		}
	}
} // HashGenerator