description = "Talk Sync Common Libraries"

dependencies {
    implementation("org.springframework.boot:spring-boot-configuration-processor")
    implementation("org.springframework:spring-core")
    implementation("org.springframework:spring-context")

    implementation("io.projectreactor:reactor-core")

    implementation("commons-codec:commons-codec:1.16.0")

    annotationProcessor("org.mapstruct:mapstruct-processor:1.5.5.Final")
    implementation("javax.xml.bind:jaxb-api:2.4.0-b180830.0359")

    // log
    implementation("org.slf4j:slf4j-api")
    implementation("org.slf4j:log4j-over-slf4j")
    implementation("org.slf4j:jul-to-slf4j")
    implementation("org.slf4j:jcl-over-slf4j")
    implementation("ch.qos.logback:logback-core")
    implementation("ch.qos.logback:logback-classic")
}
