import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    id("org.springframework.boot") version "3.2.0"
    id("io.spring.dependency-management") version "1.1.4" apply false
    id("io.freefair.lombok") version "8.4" apply false
    id("com.bmuschko.nexus") version "2.3.1" apply false
    kotlin("jvm") version "1.9.20" apply false
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
}

subprojects {
    group = "com.yulisu"
    version = "1.0-RELEASE"

    apply(plugin = "java")
    apply(plugin = "io.freefair.lombok")
    apply(plugin = "eclipse")

    repositories {
        mavenCentral()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xdiags:verbose", "-parameters")
        }
    }

    dependencies {
        implementation(platform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))

        implementation("org.springframework.boot:spring-boot-starter-webflux")

        implementation("org.slf4j:slf4j-api")
        implementation("org.slf4j:log4j-over-slf4j")
        implementation("org.slf4j:jul-to-slf4j")
        implementation("org.slf4j:jcl-over-slf4j")

        implementation("ch.qos.logback:logback-core")
        implementation("ch.qos.logback:logback-classic")

        implementation("com.google.guava:guava:32.1.1-jre")
        implementation("javax.validation:validation-api:2.0.1.Final")
    }

    tasks.register("listrepos") {
        doLast {
            println("Repositories:")
            project.repositories.forEach { repo -> println("Name: ${repo.name};") }
        }
    }
}