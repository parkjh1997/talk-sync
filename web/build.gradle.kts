description = "Web API Server"

plugins {
    id("application")
    kotlin("jvm")
}

application {
    mainClass.set("com.yulisu.web.WebApplication")
    applicationDefaultJvmArgs = listOf(
        "-server",
        "-Djava.security.egd=file:/dev/./urandom",
        "-Djava.net.preferIPv4Stack=true",
        "-Djava.awt.headless=true",
        "-Dreactor.netty.http.server.accessLogEnabled=true"
    )
}

tasks.distTar {
    into("${project.name}-${project.version}") {
        from(".")
        include("config/*")
        include("data/templates/*")
    }
}

tasks.withType<JavaCompile> {
    options.compilerArgs.add("-Xdiags:verbose")
    options.compilerArgs.add("-parameters")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

dependencies {
    implementation(project(":ts-common"))

//    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
    implementation("org.springframework.boot:spring-boot-starter-data-r2dbc")
    implementation("org.springframework.boot:spring-boot-starter-data-redis-reactive")

    // postgresql
    implementation("io.r2dbc:r2dbc-postgresql:0.8.13.RELEASE")
    implementation("org.postgresql:postgresql:42.6.0")

    // jwt
    implementation("io.jsonwebtoken:jjwt-api:0.11.5")
    implementation("io.jsonwebtoken:jjwt-impl:0.11.5")
    implementation("io.jsonwebtoken:jjwt-jackson:0.11.5")

    // swagger
//    implementation("io.springfox:springfox-boot-starter:3.0.0")
//    implementation("io.springfox:springfox-swagger-ui:3.0.0")

    // mail
    implementation("com.sun.mail:javax.mail:1.6.2")

    testImplementation("junit:junit")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("io.projectreactor:reactor-test")
}