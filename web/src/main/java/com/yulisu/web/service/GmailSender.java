package com.yulisu.web.service;

import com.google.common.base.Joiner;
import com.yulisu.web.config.MailProperties;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.event.ConnectionEvent;
import javax.mail.event.ConnectionListener;
import javax.mail.event.TransportListener;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Service
@Slf4j
public class GmailSender
{
	private static final Logger logMail = LoggerFactory.getLogger("mailLog");

	@Autowired
	private MailProperties props;
	private Session session;
	private Transport transport;

	@PostConstruct
	private void _postConstruct() throws NoSuchProviderException {
		final Properties props = new Properties();
		props.put("mail.smtp.host", this.props.getHost());
		props.put("mail.smtp.port", this.props.getPort());
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", this.props.getPort());
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		final String username = this.props.getUserId();
		final String password = new String(this.props.getPassword());

		this.session = Session.getInstance(props,
			new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
	} // _postConstruct()

	private final ConnectionListener connLsnr = new ConnectionListener() {
		@Override
		public void opened(ConnectionEvent e) {
			log.info("[Mail][Conn] opened");
		}
		@Override
		public void disconnected(ConnectionEvent e) {
			log.info("[Mail][Conn] disconnected");
		}
		@Override
		public void closed(ConnectionEvent e) {
			log.info("[Mail][Conn] closed");
		}
	};

	private synchronized Transport getTransport(TransportListener lsnr) throws MessagingException {
		Transport transport = this.transport;
		if (transport == null) {
			transport = this.session.getTransport();
			transport.addConnectionListener(connLsnr);
			if (lsnr != null) {
				transport.addTransportListener(lsnr);
			}
			transport.connect();
		}
		return transport;
	}

	/** Blocking 방식 메일 전송
	 * @param recipients 수신자 목록
	 * @param subject 제목
	 * @param content 본문
	 * @param lsnr 결과수신 Callback Listener
	 * @return
	 */
	public boolean send(Address[] recipients, String subject, String content, TransportListener lsnr) {
		return this.send(recipients, null, subject, content, lsnr);
	}
	public boolean send(Address[] recipients, Address[] ccRecipients, String subject, String content, TransportListener lsnr) {
		return this.send(recipients, ccRecipients, null, subject, content, lsnr);
	}
	public boolean send
		( Address[] recipients
		, Address[] ccRecipients
		, Address[] bccRecipients
		, String subject
		, String content
		, TransportListener lsnr
		)
	{
		String sRecipients = Joiner.on(",").join(recipients);
		try {
			final Address from = this.props.getUserName() == null
				? new InternetAddress(this.props.getUserId())
				: new InternetAddress(this.props.getUserId(), this.props.getUserName(), StandardCharsets.UTF_8.name());

			final Message message = new MimeMessage(this.session);
			message.setFrom(from);
			message.setRecipients(Message.RecipientType.TO, recipients);
			if (ccRecipients != null) message.setRecipients(Message.RecipientType.CC, ccRecipients);
			if (bccRecipients != null) message.setRecipients(Message.RecipientType.BCC, bccRecipients);
			message.setSubject(subject);
			message.setContent(content, "text/html; charset=utf-8");

			final Transport transport = this.getTransport(lsnr);
			transport.sendMessage(message, message.getAllRecipients()); // blocking....
			transport.close();
			logMail.info("{}\tSent", sRecipients);
			log.debug("Mail Sent - {}", sRecipients);
			return true;
		} catch (MessagingException | UnsupportedEncodingException e) {
			logMail.info("{}\tFail\t{}", sRecipients, e.toString());
			log.error("mail send failed.", e);
			return false;
		}
	} // send()

}
