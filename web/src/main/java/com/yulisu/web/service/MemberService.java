package com.yulisu.web.service;

import com.yulisu.web.entity.MemberEntity;
import com.yulisu.web.router.dto.ResponseDto;
import com.yulisu.web.router.dto.member.DetailDto;
import com.yulisu.web.router.dto.member.SaveDto;
import com.yulisu.web.router.dto.member.UpdateDto;
import com.yulisu.web.security.SecurityService;
import com.yulisu.web.security.model.TokenDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.query.Query;
import org.springframework.data.relational.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class MemberService {

    private final SecurityService securityService;

    private final R2dbcEntityTemplate r2dbcEntityTemplate;

    public Mono<ResponseDto> save(SaveDto dto) {
        ResponseDto responseDto = ResponseDto.getSuccessResponse();

        return getByEmail(dto.getEmail())
            .flatMap(memberEntity -> {
                final LocalDateTime now = LocalDateTime.now();

                if (memberEntity == null || memberEntity.getId() == null) {
                    final MemberEntity member = MemberEntity.of(dto);

                    return r2dbcEntityTemplate.insert(member.toBuilder()
                        .isNewEntity(true)
                        .createdAt(now)
                        .updatedAt(now)
                        .build()
                    )
                    .doOnSuccess(m -> log.info("Saved Member - {} created", m.getId()))
                    .map(m -> responseDto);
                } else if (memberEntity.getEmailVerifiedAt() == null) {
                    final Criteria criteria = Criteria.where("id").is(memberEntity.getId());
                    final Update update = Update.update("updated_at", now);

                    return r2dbcEntityTemplate.update(Query.query(criteria), update, MemberEntity.class)
                        .doOnSuccess(m -> log.info("Saved Member - {} updated", memberEntity.getId()))
                        .map(m -> responseDto);
                } else {
                    responseDto.setCode(HttpStatus.BAD_REQUEST);
                    responseDto.setMessage("중복된 이메일이 있습니다.");

                    return Mono.just(responseDto);
                }
            });
    }

    public Mono<ResponseDto> emailAuth(String email) {
        ResponseDto responseDto = ResponseDto.getSuccessResponse();

        return getByEmail(email)
            .flatMap(memberEntity -> {
                if (memberEntity != null) {

                    if (memberEntity.getEmailVerifiedAt() != null) {
                        responseDto.setCode(HttpStatus.BAD_REQUEST);
                        responseDto.setMessage("이메일 인증이 이미 완료되었습니다.");

                        return Mono.just(responseDto);
                    }

                    final LocalDateTime now = LocalDateTime.now();
                    final Criteria criteria = Criteria.where("email").is(email).and("deleted_at").isNull();
                    final Update update = Update
                        .update("email_verified_at", now)
                        .set("updated_at", now);

                    return r2dbcEntityTemplate.update(Query.query(criteria), update, MemberEntity.class)
                        .flatMap(count -> {
                            if (count <= 0) {
                                responseDto.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
                                responseDto.setMessage("이메일 인증에 실패하였습니다.");
                            }

                            return Mono.just(responseDto);
                        });
                } else {
                    responseDto.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
                    responseDto.setMessage("이메일 인증에 실패하였습니다.");

                    return Mono.just(responseDto);
                }
            });
    }

    public Mono<ResponseDto> saveInfo(UpdateDto dto) {
        ResponseDto responseDto = ResponseDto.getSuccessResponse();
        final MemberEntity member = MemberEntity.of(dto);

        return isExistsByEmail(member.getEmail())
            .flatMap(exists -> {
                if (exists) {
                    final LocalDateTime now = LocalDateTime.now();
                    final Criteria criteria = Criteria.where("email").is(member.getEmail())
                        .and("email_verified_at").isNotNull()
                        .and("deleted_at").isNull();
                    final Update update = Update.update("updated_at", now);

                    if (member.getPassword() != null) update.set("password", securityService.encodePassword(member.getPassword()));
                    if (member.getNickname() != null) update.set("nickname", member.getNickname());
                    if (member.getPosition() != null) update.set("position", member.getPosition());
                    if (member.getRealName() != null) update.set("real_name", member.getRealName());
                    if (member.getAgeConsent() != null) update.set("age_consent", member.getAgeConsent());
                    if (member.getTermsConsent() != null) update.set("terms_consent", member.getTermsConsent());
                    if (member.getPersonalConsent() != null) update.set("personal_consent", member.getPersonalConsent());
                    if (member.getNotificationsConsent() != null) update.set("notifications_consent", member.getNotificationsConsent());

                    return r2dbcEntityTemplate.update(Query.query(criteria), update, MemberEntity.class)
                        .flatMap(count -> {
                                if (count <= 0) {
                                    responseDto.setCode(HttpStatus.BAD_REQUEST);
                                    responseDto.setMessage("회원가입이 실패하였습니다.");
                                }

                            return Mono.just(responseDto);
                        });
                } else {
                    responseDto.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
                    responseDto.setMessage("회원가입이 실패하였습니다.");

                    return Mono.just(responseDto);
                }
            });
    }

    public Mono<TokenDetails> authenticate(String email, String password) {
        return getByEmail(email).flatMap(member -> securityService.authenticate(member, password));
    }

    public Mono<DetailDto> getByPrincipal(Principal principal) {
        final Long id = securityService.getMemberIdByServerRequestPrincipal(principal);

        return id != null ? getById(id) : null;
    }

    public Mono<DetailDto> getById(Long id) {
        Criteria criteria = Criteria.where("id").is(id).and("deleted_at").isNull();
        return r2dbcEntityTemplate.selectOne(Query.query(criteria), MemberEntity.class)
            .defaultIfEmpty(new MemberEntity())
            .map(DetailDto::of);
    }

    public Mono<MemberEntity> getByEmail(String email) {
        Criteria criteria = Criteria.where("email").is(email).and("deleted_at").isNull();
        return r2dbcEntityTemplate.selectOne(Query.query(criteria), MemberEntity.class)
            .defaultIfEmpty(new MemberEntity());
    }

    public Mono<TokenDetails> refreshToken(String refreshToken) {
        return securityService.validateToken(refreshToken)
            ? securityService.getNewAccessToken(refreshToken)
            : null;
    }

    public Mono<ResponseDto> changePassword(Principal principal, UpdateDto dto) {
        ResponseDto responseDto = ResponseDto.getSuccessResponse();
        final MemberEntity member = MemberEntity.of(dto);

        final LocalDateTime now = LocalDateTime.now();
        final Long id = securityService.getMemberIdByServerRequestPrincipal(principal);

        member.toBuilder()
            .id(id)
            .updatedBy(id)
            .updatedAt(now)
            .build();

        final Criteria criteria = Criteria.where("id").is(id).and("deleted_at").isNull();
        final Update update = Update.update("updated_at", now);

        return r2dbcEntityTemplate.update(Query.query(criteria), update, MemberEntity.class)
            .doOnSuccess(m -> log.info("ChangePassword - {} updated", id))
            .map(m -> responseDto);
    }

    private Mono<Boolean> isExistsById(String id) {
        Criteria criteria = Criteria.where("id").is(id).and("deleted_at").isNull();

        return r2dbcEntityTemplate.exists(Query.query(criteria), MemberEntity.class);
    }

    private Mono<Boolean> isExistsByEmail(String email) {
        Criteria criteria = Criteria.where("email").is(email).and("deleted_at").isNull();

        return r2dbcEntityTemplate.exists(Query.query(criteria), MemberEntity.class);
    }
}
