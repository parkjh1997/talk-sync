package com.yulisu.web.service;

import com.yulisu.web.entity.EmailLogEntity;
import com.yulisu.web.entity.redis.RedisEmailLogEntity;
import com.yulisu.web.router.dto.EmailLog.SaveDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.query.Query;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailLogService {

    private final R2dbcEntityTemplate r2dbcEntityTemplate;

    @Qualifier("emailLogRedisOperations")
    private final ReactiveRedisOperations<String, RedisEmailLogEntity> redisOperations;

    @Value("${web.email.sendLimitCount}")
    private int emailSendLimitCount;

    @Value("${web.email.sendLimitResetDay}")
    private int emailSendLimitResetDay;

    @Value("${web.email.verificationTimeout}")
    private int emailVerificationTimeout;

    public Mono<Boolean> isSendingExceededByEmail(String email) {
        return findRedisEmailLogEntity(email)
            .flatMap(redisEmailLogEntity -> {
                if (redisEmailLogEntity != null && redisEmailLogEntity.getEmail() != null) {
                    LocalDate today = LocalDateTime.now().toLocalDate();
                    LocalDate lastSendDate = redisEmailLogEntity.getLastSentAt().toLocalDate();

                    log.info("[isSendingExceededByEmail] - email : {}\t count : {}\t date : {}", email, redisEmailLogEntity.getCount(), lastSendDate);

                    return Mono.just(redisEmailLogEntity.getCount() >= emailSendLimitCount && today.toEpochDay() - lastSendDate.toEpochDay() < emailSendLimitResetDay);
                }

                return Mono.just(false);
            })
            .doOnError(error -> log.error("Error in isSendingExceededByEmail", error));
    }

    public Mono<Boolean> isEmailVerificationTimeExpiredByEmail(String email) {
        final LocalDateTime startOfDay = LocalDate.now().atStartOfDay().minusSeconds(emailVerificationTimeout / 1000);

        return redisOperations.opsForValue().get(email)
            .map(redisEmailLogEntity -> redisEmailLogEntity.getLastSentAt().isBefore(startOfDay));
    }

    public void save(SaveDto dto) {
        final LocalDateTime now = LocalDateTime.now();
        final Long memberId = 1L;
        final EmailLogEntity emailLogEntity = EmailLogEntity.of(dto).toBuilder()
            .isNewEntity(true)
            .createdBy(memberId)
            .createdAt(now)
            .updatedBy(memberId)
            .updatedAt(now)
            .build();

        r2dbcEntityTemplate.insert(emailLogEntity)
            .doOnSuccess(savedLog -> {
                log.info("[Saved EmailLog] - {} created", savedLog.getId());
                saveRedisEmailLogEntityToEmailLogEntity(savedLog).subscribe();
            })
            .subscribe();
    }

    public Mono<RedisEmailLogEntity> findRedisEmailLogEntity(String email) {
        return redisOperations.opsForValue().get(email)
            .defaultIfEmpty(RedisEmailLogEntity.builder().build());
    }

    public Flux<EmailLogEntity> findByCreatedAtBetween(LocalDateTime startDate, LocalDateTime endDate) {
        Criteria criteria = Criteria.where("created_at").between(startDate, endDate);

        return r2dbcEntityTemplate.select(Query.query(criteria), EmailLogEntity.class);
    }

    public Mono<Boolean> saveRedisEmailLogEntity(RedisEmailLogEntity redisEmailLogEntity) {
        // 다음날 00시에 제거되도록 duration 추가
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(now, now.toLocalDate().atStartOfDay().plusDays(1));

        return redisOperations.opsForValue()
            .set(redisEmailLogEntity.getEmail(), redisEmailLogEntity, duration)
            .doOnSuccess(success -> log.info("Successfully saved to Redis: {}", redisEmailLogEntity));
    }

    private Mono<Boolean> saveRedisEmailLogEntityToEmailLogEntity(EmailLogEntity emailLogEntity) {
        return findRedisEmailLogEntity(emailLogEntity.getEmail())
            .flatMap(redisEmailLogEntity -> {
                RedisEmailLogEntity newRedisEmailLogEntity = RedisEmailLogEntity.builder()
                    .email(emailLogEntity.getEmail())
                    .count(redisEmailLogEntity != null ? redisEmailLogEntity.getCount() + 1 : 1)
                    .lastSentAt(emailLogEntity.getCreatedAt())
                    .build();

                return saveRedisEmailLogEntity(newRedisEmailLogEntity);
            })
            .doOnError(error -> log.error("Error in saveRedisEmailLogEntity : {}", emailLogEntity.getEmail(), error));
    }
}
