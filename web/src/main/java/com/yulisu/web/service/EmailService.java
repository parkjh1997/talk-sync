package com.yulisu.web.service;

import com.yulisu.common.utils.HashGenerator;
import com.yulisu.web.router.dto.EmailLog.SaveDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import reactor.core.publisher.Mono;

import javax.mail.event.TransportEvent;
import javax.mail.event.TransportListener;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.concurrent.CountDownLatch;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailService implements TransportListener {

    private final EmailLogService emailLogService;

    private final GmailSender gmailSender;

    private final TemplateEngine templateEngine;

    private final CountDownLatch latch = new CountDownLatch(1);

    @Override
    public void messageDelivered(TransportEvent e) {
        this.latch.countDown();
    }
    @Override
    public void messageNotDelivered(TransportEvent e) {
        this.latch.countDown();
    }
    @Override
    public void messagePartiallyDelivered(TransportEvent e) {
        this.latch.countDown();
    }

    public Mono<Boolean> isSendingExceededByEmail(String email) {
        return emailLogService.isSendingExceededByEmail(email);
    }

    public Mono<Boolean> isEmailVerificationTimeExpiredByEmail(String email) {
        return emailLogService.isEmailVerificationTimeExpiredByEmail(email);
    }

    @SuppressWarnings("unchecked")
    @SneakyThrows(AddressException.class)
    @Async("emailTaskExecutor")
    public void sendVerificationEmail(String email) {
        final Context ctx = new Context();

        ctx.setVariable("linkUri", "https://talksync.kr/" + HashGenerator.encrypt(email));
        final String content = templateEngine.process("verification-email", ctx);

        String title = "[Talk Sync] 이메일 인증";
        boolean isSendSuccess = gmailSender.send(InternetAddress.parse(email), title, content, this);

        if (isSendSuccess) {
            SaveDto dto = SaveDto.builder()
                .email(email)
                .title(title)
                .content(content)
                .build();

            emailLogService.save(dto);
        }
    }
}
