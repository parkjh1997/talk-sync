package com.yulisu.web.config;

import com.yulisu.common.utils.Validator;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Component
@Data
@EqualsAndHashCode(callSuper = false)
public class MailProperties {
	private String host = "smtp.gmail.com";
	@Min(1)
	@Max(65536)
	private int port = 465;
	@Email
	private String userId = "roomcatch@gmail.com";
	@NotBlank
	private char[] password = "jbssgzcbkxnseitp".toCharArray();
	private String userName;

	@PostConstruct
	private void postConstruct() {
		if (Validator.isEmpty(this.userName)) this.userName = this.userId;
	}
}
