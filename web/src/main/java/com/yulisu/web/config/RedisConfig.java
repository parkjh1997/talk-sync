package com.yulisu.web.config;

import com.yulisu.web.entity.redis.RedisEmailLogEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private int port;

    @Bean
    @Primary
    public ReactiveRedisConnectionFactory reactiveRedisConnectionFactory() {
        return new LettuceConnectionFactory(host, port);
    }

    @Bean
    public <T> ReactiveRedisOperations<String, T> redisOperations(ReactiveRedisConnectionFactory factory, Class<T> domainClass) {
        Jackson2JsonRedisSerializer<T> serializer = new Jackson2JsonRedisSerializer<>(domainClass);

        RedisSerializationContext.RedisSerializationContextBuilder<String, T> builder =
            RedisSerializationContext.newSerializationContext(new StringRedisSerializer());

        RedisSerializationContext<String, T> context = builder
            .value(serializer)
            .hashValue(serializer)
            .hashKey(serializer)
            .build();

        return new ReactiveRedisTemplate<>(factory, context);
    }

    @Bean(name = "emailLogRedisOperations")
    public ReactiveRedisOperations<String, RedisEmailLogEntity> emailLogRedisOperations(ReactiveRedisConnectionFactory factory) {
        return redisOperations(factory, RedisEmailLogEntity.class);
    }

//    @Bean(name = "jwtRedisOperations")
//    public ReactiveRedisOperations<String, RedisJWTEntity> jwtRedisOperations(ReactiveRedisConnectionFactory factory) {
//        return redisOperations(factory, RedisJWTEntity.class);
//    }
}
