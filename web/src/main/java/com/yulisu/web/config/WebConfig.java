package com.yulisu.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring6.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.FileTemplateResolver;

@Configuration
@EnableWebFlux
@EnableAsync(proxyTargetClass = true)
public class WebConfig implements WebFluxConfigurer {

//	private JsonMapper.Builder _jsonMapperBuilder() {
//		return JsonMapper.builder()
//			// 모르는 property에 대해 무시하고 넘어간다. DTO의 하위 호환성 보장에 필요하다.
//			.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
//			// ENUM 값이 존재하지 않으면 null로 설정한다. Enum 항목이 추가되어도 무시하고 넘어가게 할 때 필요하다.
//			.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true)
//			.propertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE)
//			.serializationInclusion(JsonInclude.Include.NON_NULL)
//			.addModule(new ParameterNamesModule())
//			.addModule(new Jdk8Module())
//			.addModule(new JavaTimeModule())
//			;
//	}

//	@Bean
//	@Primary
//	ObjectMapper apiMapper() {
//		final SimpleModule module = new SimpleModule()
//			.addSerializer(ContentSource.class, new ContentSource.ApiSerializer())
//			.addSerializer(ScrapType.class, new ScrapType.ApiSerializer())
//			.addSerializer(SolutionType.class, new SolutionType.ApiSerializer())
//			.addSerializer(TopicType.class, new TopicType.ApiSerializer())
//			;
//
//		return this._jsonMapperBuilder()
//			.addModule(module)
//			.build();
//	} // apiMapper()

	@Bean
	TemplateEngine templateEngine() {
		String sTemplatePath = "data/templates/";
		return WebConfig.createTemplateEngine(sTemplatePath);
	} // templateEngine()

	public static TemplateEngine createTemplateEngine(String templatePath) {
		final FileTemplateResolver templateResolver = new FileTemplateResolver();
		// HTML is the default mode, but we set it anyway for better understanding of code
		templateResolver.setTemplateMode(TemplateMode.HTML);
		// This will convert "home" to "/WEB-INF/templates/home.html"
		templateResolver.setPrefix(templatePath);
		templateResolver.setSuffix(".html");

		// Template cache TTL=1h. If not set, entries would be cached until expelled by LRU

		// Cache is set to true by default. Set to false if you want templates to
		// be automatically updated when modified.
		templateResolver.setCacheable(false); // 현재, 드문드문 메일 발송할 때나 본문 템플릿 읽는 수준이라, false 설정.

		final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		templateEngine.setEnableSpringELCompiler(true);
		return templateEngine;
	} // createTemplateEngine()

//	/** 내부 시스템 호출용 ObjectMapper<p>
//	 * @return
//	 */
//	@Bean
//	ObjectMapper requestMapper() {
//		return this._objectMapper()
//			.propertyNamingStrategy(PropertyNamingStrategies.LOWER_CAMEL_CASE)
//			.build();
//	} // compaignApiMapper()

//	@Override
//	public void addFormatters(FormatterRegistry registry) {
//		registry.addConverter(new Converter<String, ContentSource>() {
//			@Override
//			public ContentSource convert(String source) {
//				return ContentSource.of(source);
//			}
//		});
//
//		registry.addConverter(new Converter<String, ScrapType>() {
//			@Override
//			public ScrapType convert(String source) {
//				return ScrapType.of(source);
//			}
//		});
//
//		registry.addConverter(new Converter<String, SolutionType>() {
//			@Override
//			public SolutionType convert(String source) {
//				return SolutionType.of(source);
//			}
//		});
//
//		registry.addConverter(new Converter<String, TopicType>() {
//			@Override
//			public TopicType convert(String source) {
//				return TopicType.of(source);
//			}
//		});
//	} // addFormatters()

	@Bean
	public ThreadPoolTaskExecutor emailTaskExecutor() {
		final ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setCorePoolSize(5);
		taskExecutor.setMaxPoolSize(25);
		taskExecutor.setQueueCapacity(3000);
		taskExecutor.setThreadNamePrefix("EmailTaskExecutor-");
		taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
		taskExecutor.setAwaitTerminationSeconds(30);
		taskExecutor.initialize();
		return taskExecutor;
	}

} // WebConfig
