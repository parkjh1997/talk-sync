package com.yulisu.web.handler;

import com.yulisu.common.utils.HashGenerator;
import com.yulisu.web.router.dto.ResponseDto;
import com.yulisu.web.router.dto.member.RefreshDto;
import com.yulisu.web.router.dto.member.SaveDto;
import com.yulisu.web.router.dto.member.UpdateDto;
import com.yulisu.web.security.model.AuthRequest;
import com.yulisu.web.security.model.AuthResponse;
import com.yulisu.web.service.EmailService;
import com.yulisu.web.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class MemberHandler {

    private final MemberService memberService;

    private final EmailService emailService;

    public Mono<ServerResponse> registerEmail(ServerRequest request) {
        return request.bodyToMono(SaveDto.class)
            .flatMap(dto -> emailService.isSendingExceededByEmail(dto.getEmail())
                .flatMap(isEmailLimitExceeded -> {
                    if (isEmailLimitExceeded) {
                        ResponseDto responseDto = ResponseDto.builder()
                            .code(HttpStatus.TOO_MANY_REQUESTS)
                            .message("발송 횟수를 초과하였습니다.")
                            .build();

                        return ServerResponse.badRequest().body(Mono.just(responseDto), ResponseDto.class);
                    } else {
                        return memberService.save(dto)
                            .flatMap(responseDto -> {
                                if ("200".equals(responseDto.getCode())) {
                                    emailService.sendVerificationEmail(dto.getEmail());
                                    return ServerResponse.ok().body(Mono.just(responseDto), ResponseDto.class);
                                } else {
                                    return ServerResponse.badRequest().body(Mono.just(responseDto), ResponseDto.class);
                                }
                            });
                    }
                })
            );
    }

    public Mono<ServerResponse> emailAuth(ServerRequest request) {
        ResponseDto responseDto = ResponseDto.getSuccessResponse();

        return request.bodyToMono(UpdateDto.class)
            .flatMap(dto -> {
                final String email = HashGenerator.decrypt(dto.getEmail());

                return emailService.isEmailVerificationTimeExpiredByEmail(email)
                    .flatMap(isExpired -> {
                        if (isExpired) {
                            return memberService.emailAuth(email);
                        } else {
                            responseDto.setCode(HttpStatus.BAD_REQUEST);
                            responseDto.setMessage("이메일 인증 시간이 만료되었습니다.");

                            return Mono.just(responseDto);
                        }
                    });
            })
            .flatMap(dto -> dto.getCode().is2xxSuccessful()
                ? ServerResponse.ok().body(Mono.just(dto), ResponseDto.class)
                : ServerResponse.badRequest().body(Mono.just(dto), ResponseDto.class)
            );
    }

    public Mono<ServerResponse> saveInfo(ServerRequest request) {
        return request.bodyToMono(UpdateDto.class)
            .flatMap(dto -> memberService.saveInfo(dto)
                .flatMap(responseDto -> {
                    if (responseDto.getCode().is2xxSuccessful()) {
                        return ServerResponse.ok().body(Mono.just(responseDto), ResponseDto.class);
                    } else {
                        return ServerResponse.badRequest().body(Mono.just(responseDto), ResponseDto.class);
                    }
                })
            );
    }

    public Mono<ServerResponse> login(ServerRequest request) {
        return request.bodyToMono(AuthRequest.class)
            .flatMap(authRequest -> memberService.authenticate(authRequest.getEmail(), authRequest.getPassword()))
            .flatMap(tokenDetails -> Mono.just(AuthResponse.builder()
                .id(tokenDetails.getMemberId())
                .accessToken(tokenDetails.getAccessToken())
                .refreshToken(tokenDetails.getRefreshToken())
                .build())
            )
            .flatMap(authResponseDto -> ServerResponse.ok().bodyValue(authResponseDto));
    }

    public Mono<ServerResponse> refresh(ServerRequest request) {
        return request.bodyToMono(RefreshDto.class)
            .flatMap(refreshDto -> memberService.refreshToken(refreshDto.getRefreshToken()))
            .flatMap(tokenDetails -> tokenDetails != null
                ? Mono.just(AuthResponse.builder()
                    .id(tokenDetails.getMemberId())
                    .accessToken(tokenDetails.getAccessToken())
                    .refreshToken(tokenDetails.getRefreshToken())
                    .build()
                )
                : null
            )
            .flatMap(authResponseDto -> authResponseDto != null
                ? ServerResponse.ok().bodyValue(authResponseDto)
                : ServerResponse.status(HttpStatus.UNAUTHORIZED).build()
            );
    }

    public Mono<ServerResponse> getUserInfo(ServerRequest request) {
        return request.principal()
            .flatMap(principal -> memberService.getByPrincipal(principal))
            .flatMap(result -> ServerResponse.ok().bodyValue(result));
    }

    public Mono<ServerResponse> changePassword(ServerRequest request) {
        return request.principal()
            .flatMap(principal -> request.bodyToMono(UpdateDto.class)
                .flatMap(dto -> memberService.changePassword(principal, dto))
            )
            .flatMap(authResponseDto -> authResponseDto != null
                ? ServerResponse.ok().bodyValue(authResponseDto)
                : ServerResponse.status(HttpStatus.UNAUTHORIZED).build()
            );
    }
}
