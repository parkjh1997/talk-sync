package com.yulisu.web.handler;

import com.yulisu.web.entity.EmailLogEntity;
import com.yulisu.web.entity.redis.RedisEmailLogEntity;
import com.yulisu.web.service.EmailLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.Comparator;

@Component
@RequiredArgsConstructor
public class DataLoadHandler {

    private final EmailLogService emailLogService;

    public Flux<Boolean> loadEmailLogsFromDBToRedis() {
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = today.plusDays(1);

        return emailLogService.findByCreatedAtBetween(today.atStartOfDay(), tomorrow.atStartOfDay())
            .groupBy(EmailLogEntity::getEmail)
            .flatMap(stringEmailLogEntityGroupedFlux -> stringEmailLogEntityGroupedFlux
                .collectList()
                .map(emailLogEntities -> RedisEmailLogEntity.builder()
                    .email(stringEmailLogEntityGroupedFlux.key())
                    .count(emailLogEntities.size())
                    .lastSentAt(emailLogEntities.stream().map(EmailLogEntity::getCreatedAt).max(Comparator.naturalOrder()).get())
                    .build()
                )
            )
            .flatMap(redisEmailLogEntity -> emailLogService.saveRedisEmailLogEntity(redisEmailLogEntity));
    }
}
