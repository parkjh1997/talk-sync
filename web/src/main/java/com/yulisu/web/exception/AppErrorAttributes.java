package com.yulisu.web.exception;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class AppErrorAttributes extends DefaultErrorAttributes {


    public AppErrorAttributes() {
        super();
    }

    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

        var errorAttributes = super.getErrorAttributes(request, ErrorAttributeOptions.defaults());
        var error = getError(request);
        var errorMap = new LinkedHashMap<String, Object>();
        var errorList = new ArrayList<Map<String, Object>>();

        if (
            error instanceof AuthException ||
            error instanceof UnauthorizedException ||
            error instanceof ExpiredJwtException ||
            error instanceof MalformedJwtException
        ) {
            status = HttpStatus.UNAUTHORIZED;
            errorMap.put("code", status.value());
            errorMap.put("message", error.getMessage());
        } else if (error instanceof ApiException) {
            status = HttpStatus.BAD_REQUEST;
            errorMap.put("code", status.value());
            errorMap.put("message", error.getMessage());
        } else {
            errorMap.put("code", HttpStatus.INTERNAL_SERVER_ERROR.value());
            errorMap.put("message", error.getMessage() != null ? error.getMessage() : error.getClass().getName());
        }

        errorList.add(errorMap);

        var errors = new HashMap<String, Object>();
        errors.put("errors", errorList);
        errorAttributes.put("status", status.value());
        errorAttributes.put("errors", errors);

        return errorAttributes;
    }
}

