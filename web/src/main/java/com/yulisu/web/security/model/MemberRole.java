package com.yulisu.web.security.model;

import lombok.Getter;

@Getter
public enum MemberRole {
    MEMBER,
    ADMIN;

    private final String lowerName;
    private final String upperName;

    MemberRole() {
        this.lowerName = name().toLowerCase();
        this.upperName = name().toUpperCase();
    }
}
