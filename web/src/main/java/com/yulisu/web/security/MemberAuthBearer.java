package com.yulisu.web.security;

import com.yulisu.web.security.model.CustomPrincipal;
import io.jsonwebtoken.Claims;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import reactor.core.publisher.Mono;

import java.util.List;

public class MemberAuthBearer {

    public static Mono<Authentication> create(JwtHandler.VerificationResult verificationResult) {
        Claims claims = verificationResult.claims;
        Long principalId = Long.parseLong(claims.getSubject());

        String role = claims.get("role", String.class);
        String memberName = claims.get("memberName", String.class);

        List<SimpleGrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(role));
        CustomPrincipal principal = new CustomPrincipal(principalId, memberName);

        return Mono.justOrEmpty(new UsernamePasswordAuthenticationToken(principal, null, authorities));
    }
}
