package com.yulisu.web.security;

import com.yulisu.web.entity.MemberEntity;
import com.yulisu.web.exception.AuthException;
import com.yulisu.web.security.model.CustomPrincipal;
import com.yulisu.web.security.model.TokenDetails;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class SecurityService {

    private final BCryptPasswordEncoder passwordEncoder;

    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.accessExpiration}")
    private Integer accessExpirationInSeconds;
    @Value("${jwt.refreshExpiration}")
    private Integer refreshExpirationInSeconds;
    @Value("${jwt.issuer}")
    private String issuer;

    public Mono<TokenDetails> authenticate(MemberEntity member, String password) {
        if (member == null) {
            return Mono.error(new AuthException("Invalid email", "PROSELYTE_INVALID_EMAIL"));
        } else if (member.getEmailVerifiedAt() == null) {
            return Mono.error(new AuthException("Email verification required", "PROSELYTE_EMAIL_VERIFICATION_REQUIRED"));
        } else if (member.isDeleted()) {
            return Mono.error(new AuthException("Account disabled", "PROSELYTE_MEMBER_ACCOUNT_DISABLED"));
        } else if (!passwordEncoder.matches(password, member.getPassword())) {
            return Mono.error(new AuthException("Invalid password", "PROSELYTE_INVALID_PASSWORD"));
        }

        return Mono.just(generateToken(member).toBuilder()
            .memberId(member.getId().toString())
            .build()
        );
    }

    private TokenDetails generateToken(MemberEntity member) {
        Map<String, Object> claims = new HashMap<>() {{
            put("id", member.getId());
            put("role", member.getRole());
            put("email", member.getEmail());
        }};
        return generateToken(claims, member.getId().toString());
    }

    private TokenDetails generateToken(Map<String, Object> claims, String subject) {
        final Date now = new Date();

        return TokenDetails.builder()
            .accessToken(generateAccessToken(claims, subject, now))
            .refreshToken(generateRefreshToken(claims, subject, now))
            .build();
    }

    public Long getMemberIdByServerRequestPrincipal(Principal principal) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) principal;
        CustomPrincipal customPrincipal = (CustomPrincipal) usernamePasswordAuthenticationToken.getPrincipal();

        return customPrincipal.getId();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(secret.getBytes()).build().parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Mono<TokenDetails> getNewAccessToken(String token) {
        final Date now = new Date();
        Map<String, Object> claims = Jwts.parserBuilder()
            .setSigningKey(secret.getBytes())
            .build()
            .parseClaimsJws(token)
            .getBody();

        final String memberId = claims.get("sub").toString();

        return Mono.just(TokenDetails.builder()
            .memberId(memberId)
            .accessToken(generateAccessToken(claims, memberId, now))
            .refreshToken(token)
            .build()
        );
    }

    private String generateAccessToken(Map<String, Object> claims, String subject, Date now) {
        final Long accessExpirationTimeInMillis = accessExpirationInSeconds * 1000L;

        return Jwts.builder()
            .setClaims(claims)
            .setIssuer(issuer)
            .setSubject(subject)
            .setIssuedAt(now)
            .setId(UUID.randomUUID().toString())
            .setExpiration(new Date(new Date().getTime() + accessExpirationTimeInMillis))
            .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encodeToString(secret.getBytes()))
            .compact();
    }

    private String generateRefreshToken(Map<String, Object> claims, String subject, Date now) {
        final Long refreshExpirationTimeInMillis = refreshExpirationInSeconds * 1000L;

        return Jwts.builder()
            .setClaims(claims)
            .setIssuer(issuer)
            .setSubject(subject)
            .setIssuedAt(now)
            .setId(UUID.randomUUID().toString())
            .setExpiration(new Date(new Date().getTime() + refreshExpirationTimeInMillis))
            .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encodeToString(secret.getBytes()))
            .compact();
    }

    public String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }
}
