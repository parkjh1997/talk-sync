package com.yulisu.web.security;

import com.yulisu.web.exception.UnauthorizedException;
import com.yulisu.web.router.dto.member.DetailDto;
import com.yulisu.web.security.model.CustomPrincipal;
import com.yulisu.web.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class AuthManager implements ReactiveAuthenticationManager {

    private final MemberService memberService;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        CustomPrincipal principal = (CustomPrincipal) authentication.getPrincipal();

        return memberService.getById(principal.getId())
            .filter(DetailDto::isNotDeleted)
            .switchIfEmpty(Mono.error(new UnauthorizedException("User disabled")))
            .map(dto -> authentication);
    }
}
