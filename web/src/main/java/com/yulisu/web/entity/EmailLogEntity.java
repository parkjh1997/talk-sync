package com.yulisu.web.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.yulisu.web.router.dto.EmailLog.SaveDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.relational.core.mapping.Table;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Table("email_log")
public class EmailLogEntity extends BaseEntity {

    private String email;

    private String title;
    private String content;

    public static EmailLogEntity of(SaveDto dto) {
        return EmailLogEntity.builder()
            .email(dto.getEmail())
            .title(dto.getTitle())
            .content(dto.getContent())
            .build();
    }
}
