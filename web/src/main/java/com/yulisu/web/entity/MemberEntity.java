package com.yulisu.web.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.yulisu.web.router.dto.member.SaveDto;
import com.yulisu.web.router.dto.member.UpdateDto;
import com.yulisu.web.security.model.MemberRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Table("member")
public class MemberEntity extends BaseEntity {

    private MemberRole role;
    private String email;
    private String password;
    private String nickname;

    private String position;

    private String realName;

    private String googleAuthKey;

    private LocalDateTime emailVerifiedAt;

    private Boolean ageConsent;
    private Boolean termsConsent;
    private Boolean personalConsent;
    private Boolean notificationsConsent;

    public static MemberEntity of(SaveDto dto) {
        return MemberEntity.builder()
            .id(dto.getId())
            .email(dto.getEmail())
            .password(dto.getPassword())
            .nickname(dto.getNickname())
            .position(dto.getPosition())
            .realName(dto.getRealName())
            .ageConsent(dto.getAgeConsent())
            .termsConsent(dto.getTermsConsent())
            .personalConsent(dto.getPersonalConsent())
            .notificationsConsent(dto.getNotificationsConsent())
            .build();
    }

    public static MemberEntity of(UpdateDto dto) {
        return MemberEntity.builder()
            .id(dto.getId())
            .email(dto.getEmail())
            .password(dto.getPassword())
            .nickname(dto.getNickname())
            .position(dto.getPosition())
            .realName(dto.getRealName())
            .ageConsent(dto.getAgeConsent())
            .termsConsent(dto.getTermsConsent())
            .personalConsent(dto.getPersonalConsent())
            .notificationsConsent(dto.getNotificationsConsent())
            .build();
    }
}
