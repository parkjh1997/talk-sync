package com.yulisu.web.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;

import java.sql.Timestamp;
import java.time.LocalDateTime;
@Getter
@Setter
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public abstract class BaseEntity implements Persistable<Long> {

    @Id
    private Long id;

    private Long createdBy;
    private LocalDateTime createdAt;
    public Long getTimeForCreatedAt() {
        return Timestamp.valueOf(createdAt).getTime();
    }

    private Long updatedBy;
    private LocalDateTime updatedAt;
    public Long getTimeForUpdatedAt() {
        return Timestamp.valueOf(updatedAt).getTime();
    }

    private Long deletedBy;
    private LocalDateTime deletedAt;
    public Long getTimeForDeletedAt() {
        return Timestamp.valueOf(deletedAt).getTime();
    }

    @Transient
    private boolean isNewEntity;

    public void setIsNewEntity(boolean isNewEntity) {
        this.isNewEntity = isNewEntity;
    }

    public boolean isNew() {
        return this.isNewEntity;
    }

    public boolean isDeleted() {
        return this.deletedBy != null;
    }
    public boolean isNotDeleted() {
        return !this.isDeleted();
    }
}
