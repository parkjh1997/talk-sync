package com.yulisu.web.router;

import com.yulisu.web.router.dto.ResponseDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Configuration
@EnableWebFlux
public class WebRouter {

    @Bean
    public RouterFunction<ServerResponse> webRoutes() {
        return RouterFunctions.route()
            .GET("/ping", this::ping)
            .build();
    }

    private Mono<ServerResponse> ping(ServerRequest serverRequest) {
        return ServerResponse.ok()
            .bodyValue(ResponseDto.getSuccessResponse());
    }
}