package com.yulisu.web.router.dto.member;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.yulisu.web.entity.MemberEntity;
import com.yulisu.web.security.model.MemberRole;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class DetailDto {

    private Long id;

    private MemberRole role;

    private String email;
    private String nickname;
    private String position;

    private Long createdBy;
    private LocalDateTime createdAt;

    private LocalDateTime deletedAt;

    public static DetailDto of(MemberEntity entity) {
        return DetailDto.builder()
            .id(entity.getId())
            .role(entity.getRole())
            .email(entity.getEmail())
            .nickname(entity.getNickname())
            .position(entity.getPosition())
            .createdBy(entity.getCreatedBy())
            .createdAt(entity.getCreatedAt())
            .deletedAt(entity.getDeletedAt())
            .build();
    }

    public static boolean isDeleted(DetailDto dto) {
        return dto.deletedAt != null;
    }

    public static boolean isNotDeleted(DetailDto dto) {
        return !isDeleted(dto);
    }
}
