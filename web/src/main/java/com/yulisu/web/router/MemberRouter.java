package com.yulisu.web.router;

import com.yulisu.web.handler.MemberHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
@EnableWebFlux
@RequiredArgsConstructor
public class MemberRouter {

    private final MemberHandler memberHandler;

    @Bean
    public RouterFunction<ServerResponse> memberRoutes() {
        return RouterFunctions.route()
            .path("/member", builder -> builder
                .POST("/register/email", memberHandler::registerEmail)      /** 회원가입 (이메일 인증 메일 발송) */
                .POST("/register/email-auth", memberHandler::emailAuth)     /** 회원가입 (이메일 인증) */
                .POST("/register/info", memberHandler::saveInfo)            /** 회원가입 (회원정보) */
                .POST("/login", memberHandler::login)                       /** 로그인 */
                .POST("/refresh", memberHandler::refresh)                   /** 토큰 재발행 */
                .GET("/info", memberHandler::getUserInfo)                   /** 회원정보 조회 */
                .PUT("/password", memberHandler::changePassword)            /** 비밀번호 변경 */
            )
            .build();
    }
}