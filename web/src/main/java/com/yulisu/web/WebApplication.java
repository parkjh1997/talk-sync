package com.yulisu.web;

import com.yulisu.web.handler.DataLoadHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.reactive.config.EnableWebFlux;

import java.util.Optional;

@SpringBootApplication(scanBasePackages = "com.yulisu", exclude = { SecurityAutoConfiguration.class, ThymeleafAutoConfiguration.class })
@EnableWebFlux
@EnableAsync(proxyTargetClass = true)
@RequiredArgsConstructor
@Slf4j
public class WebApplication implements ApplicationRunner {

    private final DataLoadHandler dataLoadHandler;

    @Override
    public void run(ApplicationArguments args) {
        final Optional<String> cmdLine = ProcessHandle.current().info().commandLine();
        log.info("Start\t{}\t{}", ProcessHandle.current().pid(), cmdLine.isPresent() ? cmdLine.get() : "");
        dataLoadHandler.loadEmailLogsFromDBToRedis()
            .doOnError(error -> log.error("[WebApplication - {}] - {}", "loadEmailLogsFromDBToRedis", error.getMessage(), error))
            .subscribe();
    }

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

}
